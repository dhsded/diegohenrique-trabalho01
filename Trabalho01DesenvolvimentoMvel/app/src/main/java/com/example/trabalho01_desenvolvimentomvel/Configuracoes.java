package com.example.trabalho01_desenvolvimentomvel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Configuracoes extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracoes);
    }

    public void irParaTela1 (View view){
        Intent intent5 = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent5);

    }

    public void irParaTela2 (View view){
        Intent intent6 = new Intent(getApplicationContext(),Cadastro.class);
        startActivity(intent6);


    }
}