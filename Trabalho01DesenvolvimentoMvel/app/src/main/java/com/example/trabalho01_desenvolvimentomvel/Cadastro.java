package com.example.trabalho01_desenvolvimentomvel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Cadastro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
    }

    public void irParaTela1 (View view){
        Intent intent3 = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent3);

    }

    public void irParaTela3 (View view){
        Intent intent4 = new Intent(getApplicationContext(),Configuracoes.class);
        startActivity(intent4);

    }


}